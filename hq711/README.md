###所有字符编码要求是utf8，换行要求是\n
###（windows默认编码是gbk，换行也多了个\r,所以如果非要在windows下用，下个编辑器，notepad++之类的，设置好字符编码和换行）



商品列表为class*_page*.xml, (*表示分类号、页数)
+ 对应分类没有商品，也要有个空文件class*_page1.xml，表示第一页商品列表就已经是空的了。
+ 假设对应分类有两页商品列表，也要有个空文件class*_page3.xml，表示第三页商品列表为空。    
（不然客户端会提示网络异常）。    
class1:食品、饮料    
class2:烟酒    
class3:家居    
class4:数码    
class5:文具    
class6:缴费    
class7:其他    

链接地址为http://182.92.214.222/hq711/calss*_page*.xml    
```
<goodslist>
<goods>
<goods_name>可口可乐1</goods_name><!--商品名称-->
<goods_image>http://182.92.214.222/hq711/pic/1.png</goods_image><!--商品缩略图的网址-->
<goods_detail>http://182.92.214.222/hq711/html/goods_detail_001.html</goods_detail><!--详情的网址-->
<introduction>可口可乐 易拉罐装</introduction><!--简介-->
<price>1元/罐</price><!--单价-->
<price_float>1</price_float><!--不带单位的单价（可以从String转为float）-->
</goods>
</goodslist>
```
pic文件夹放商品图片，    

图片的链接为：    
http://182.92.214.222/hq711/pic/图片名（图片名最好要英文）    
    
    
html文件夹主要放商品详情的页面，还有商店简介的页面等    


