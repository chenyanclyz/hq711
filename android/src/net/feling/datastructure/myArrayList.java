package net.feling.datastructure;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import android.os.Bundle;


/**
 * 这个文件没有用
 * 
* 重写的方法在程序重启后不生效。所以不用这个类了（不在类里面处理数据）。改为普通的Arraylist，在类外处理好数据（Config.toXML）再传进来
*/
public class myArrayList<Bundle> extends ArrayList<Bundle> {


		@Override
		public String toString() {
			String s1 = "<goodslist>";
			String s2 = "";
			String s3 = "</goodslist>";
			for(int i=0;i<this.size(); i++){
				String s21 = "<goods>";
				String s22 = "";
				String s23 = "</goods>";
				Set<String> keyset = ((android.os.Bundle) this.get(i)).keySet();
				Iterator<String> it = keyset.iterator();
				while(it.hasNext()){
					String key = it.next();
					if ("number".equals(key)){
						s22 = s22 + "<"+key+">" +String.valueOf(((android.os.Bundle) this.get(i)).getInt(key)) + "</"+key+">";
					}else{
					s22 = s22 + "<"+key+">" +((android.os.Bundle) this.get(i)).getString(key) + "</"+key+">";
					}
				}
			s2 = s2 + s21 + s22 + s23;
			}
			return s1 + s2 + s3;
		};
		
		@Override
		public boolean contains(Object object) {
			if (object != null) {
				for (int i = 0; i < this.size(); i++) {
					if (((android.os.Bundle) object).get("goods_name").equals(
							(((android.os.Bundle) this.get(i)).get("goods_name")))) {
						return true;
					}
				}
			} else {
				for (int i = 0; i < this.size(); i++) {
					if (this.get(i) == null) {
						return true;
					}
				}
			}
			return false;
		};

		@Override
		public boolean add(Bundle object) {
			
			if (!this.contains(object)) {
				((android.os.Bundle) object).putInt("number", 1);
				return super.add(object);
			}else {
				return true;
			}
		}

}
