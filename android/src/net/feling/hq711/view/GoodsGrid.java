package net.feling.hq711.view;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.widget.AbsListView.OnScrollListener;
import android.widget.AbsListView;
import net.feling.hq711.GoodsDetailActivity;
import net.feling.hq711.GoodslistActivity;
import net.feling.hq711.R;
import net.feling.hq711.web.HttpFetcher;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class GoodsGrid extends GridView implements OnScrollListener {
	private myLoadListener myloadlistener;
	int totalItemCount, lastVisibleItem, firstVisibleItem;
	private boolean isLaodingmore = false;
	private OnItemClickListener onitemclicklistener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			Intent intent = new Intent();
			intent.setClass(getContext(), GoodsDetailActivity.class);
			Bundle bundle_data = new Bundle(); 
			Set<String> keyset = data.get(position).keySet();
			Iterator it = keyset.iterator();
			while (it.hasNext()){
				String key = (String) it.next();
				bundle_data.putString(key, data.get(position).get(key));
			}
			intent.putExtra("goods", bundle_data);
			getContext().startActivity(intent);

		}
	};

	private List<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();

	public void setData(List<HashMap<String, String>> data) {
		this.data.clear();
		for (int i = 0; i < data.size(); i++) {
			String url = data.get(i).get("goods_image");
			if (NetPicSimpleAdapter.imagecache.get(url) == null) {
				new NetPicTask().execute(url);
			}
			this.data.add(data.get(i));
		}
		netpicsimpleadapter.notifyDataSetChanged();
	}

	public void addData(List<HashMap<String, String>> data) {
		for (int i = 0; i < data.size(); i++) {
			String url = data.get(i).get("goods_image");
			if (NetPicSimpleAdapter.imagecache.get(url) == null) {
				new NetPicTask().execute(url);
			}
			this.data.add(data.get(i));
		}
		netpicsimpleadapter.notifyDataSetChanged();
	}

	NetPicSimpleAdapter netpicsimpleadapter = new NetPicSimpleAdapter(
			getContext(), data, R.layout.item_goodslist, new String[] {
					"goods_image", "introduction", "price" }, new int[] {
					R.id.goods_image, R.id.introduction, R.id.price });

	public GoodsGrid(Context context) {
		super(context);
		this.setAdapter(this.netpicsimpleadapter);
		this.setOnItemClickListener(this.onitemclicklistener);
		this.setOnScrollListener(this);
	}

	public GoodsGrid(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.setAdapter(this.netpicsimpleadapter);
		this.setOnItemClickListener(this.onitemclicklistener);
		this.setOnScrollListener(this);
	}

	public GoodsGrid(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.setAdapter(this.netpicsimpleadapter);
		this.setOnItemClickListener(this.onitemclicklistener);
		this.setOnScrollListener(this);
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// this.scrollState = scrollState;
		if ((totalItemCount -  lastVisibleItem <=2 )
				&& scrollState == SCROLL_STATE_IDLE) {
			if (!isLaodingmore) {
				isLaodingmore = true;
				this.myloadlistener.onLoadMore();
			}
		}
	}

	public void loadComplete() {
		isLaodingmore = false;
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		this.lastVisibleItem = firstVisibleItem + visibleItemCount;
		this.totalItemCount = totalItemCount;
		this.firstVisibleItem = firstVisibleItem;

	}

	public void setInterface(myLoadListener myloadlistener) {
		this.myloadlistener = myloadlistener;
	}

	public interface myLoadListener {
		public void onLoadMore();
	}

	private class NetPicTask extends AsyncTask<String, Void, Bitmap> {

		private String url;

		@Override
		protected void onPreExecute() {
		}

		@Override
		protected Bitmap doInBackground(String... urls) {
			url = urls[0];
			InputStream is = new HttpFetcher().getStream(urls[0]);
			if (is != null) {
				return BitmapFactory.decodeStream(is);
			} else {
				return null;
			}
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			if (result != null) {
				NetPicSimpleAdapter.imagecache.put(url, result);
				// System.out.print("图片");
				// System.out.println(url);
				netpicsimpleadapter.notifyDataSetChanged();
			}

		}
	}

}
