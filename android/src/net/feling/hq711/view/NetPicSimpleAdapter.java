package net.feling.hq711.view;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import net.feling.hq711.R;
import net.feling.hq711.web.HttpFetcher;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleAdapter;

public class NetPicSimpleAdapter extends SimpleAdapter {
	private ImageView imageview;
	WeakHashMap<Integer, View> map = new WeakHashMap<Integer, View>();// 防止错位
	public static HashMap<String, Bitmap> imagecache = new HashMap<String, Bitmap>();// 图片缓存

	public NetPicSimpleAdapter(Context context,
			List<? extends Map<String, ?>> data, int resource, String[] from,
			int[] to) {
		super(context, data, resource, from, to);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = map.get(position);// 防止错位
		View v = super.getView(position, convertView, parent);
		map.put(position, convertView);
		return v;
	}

	@Override
	public void setViewImage(ImageView v, String value) {
		if (v.getId() == R.id.goods_image || v.getId() == R.id.goods_image_trolley) {
			if (imagecache.get(value) != null) {
				v.setImageBitmap(imagecache.get(value));
			} 
			//每次图片添加到缓存里的时候后都会更新一次界面，所以，不会有
			//图片不再缓存的情况发生
//				else {
//				imageview = v;
//				String url = value;
//				System.out.print("获取图片");
//				System.out.println(value);
//				new NetPicTask().execute(url);
//			}
		} else {
			super.setViewImage(v, value);
		}
	}

//	private class NetPicTask extends AsyncTask<String, Void, Bitmap> {
//
//		private String url;
//
//		@Override
//		protected void onPreExecute() {
//		}
//
//		@Override
//		protected Bitmap doInBackground(String... urls) {
//			url = urls[0];
//			InputStream is = new HttpFetcher().getStream(urls[0]);
//			return BitmapFactory.decodeStream(is);
//		}
//
//		@Override
//		protected void onPostExecute(Bitmap result) {
//			imageview.setImageBitmap(result);
//			imagecache.put(url, result);
//		}
//	}

}