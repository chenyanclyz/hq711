package net.feling.hq711;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.feling.datastructure.myArrayList;
import net.feling.hq711.view.NetPicSimpleAdapter;
import net.feling.hq711.web.XMLHandler;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 
 * @author chenyan@feling.net 购物车界面， 回时需要finish
 *
 */
public class TrolleyActivity extends Activity {

	private ListView listview;
	private Button button;
	private TextView textview;
	private List<? extends Map<String, ?>> data;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_trolley);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		InitView();
		setAddupto();
	}

	private void InitView() {
		textview = (TextView) findViewById(R.id.TextView_addupto_trolley);
		button = (Button) findViewById(R.id.Button_setorder_trolley);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(TrolleyActivity.this,
						OrderActivity.class);
				Bundle goodslist_bundle = new Bundle();
				ArrayList<Bundle> temp_goods_in_trolley = new ArrayList<Bundle>();
				for (int i=0;i<Config.goods_in_trolley.size(); i++){
					if (Config.goods_in_trolley.get(i).getInt("number") != 0){
						temp_goods_in_trolley.add(Config.goods_in_trolley.get(i));
					}
				}
				if (temp_goods_in_trolley.size() == 0){
					Toast.makeText(TrolleyActivity.this, R.string.no_goods_selected,
							Toast.LENGTH_SHORT).show();
					return;
				}
				goodslist_bundle.putSerializable("goodslist", temp_goods_in_trolley);
				intent.putExtra("goodslist_bundle", goodslist_bundle);
				startActivity(intent);	
				finish();
			}
		});

		listview = (ListView) findViewById(R.id.ListView_trolley);
		data = get_goods_in_trolley();
		final NetPicSimpleAdapter adapter = new NetPicSimpleAdapter(this, data,
				R.layout.item_trolleylist, new String[] { "goods_image",
						"price", "introduction", "number" }, new int[] {
						R.id.goods_image_trolley, R.id.goods_price_trolley,
						R.id.goods_introduction_trolley,
						R.id.goods_number_trolley });
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent intent = new Intent();
				intent.setClass(TrolleyActivity.this, GoodsDetailActivity.class);
				intent.putExtra("goods", Config.goods_in_trolley.get(position));
				startActivity(intent);
			}

		});
		listview.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				final NumberPicker numberpicker = new NumberPicker(
						TrolleyActivity.this);
				numberpicker.setMinValue(0);
				numberpicker.setMaxValue(500);
				numberpicker.setValue(Config.goods_in_trolley.get(position).getInt(
						"number"));
				final int i = position;

				new AlertDialog.Builder(TrolleyActivity.this)
						.setTitle(
								TrolleyActivity.this
										.getString(R.string.title_dialog_trolley))
						.setView(numberpicker)
						.setPositiveButton(
								TrolleyActivity.this
										.getString(R.string.buttonr_dialog_trolley),
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										Config.goods_in_trolley.get(i).putInt(
												"number",
												numberpicker.getValue());
										SharedPreferences preferences = getSharedPreferences("trolley", 0);
										Editor editor = preferences.edit();
										editor.putString("trolley", Config.toXML(Config.goods_in_trolley));
										editor.commit();
										Intent intent = new Intent();
										intent.setClass(TrolleyActivity.this,
												TrolleyActivity.class);
										startActivity(intent);
										finish();
										// data = get_goods_in_trolley();
										// adapter.notifyDataSetChanged();

									}
								})
						.setNegativeButton(
								TrolleyActivity.this
										.getString(R.string.buttonl_dialog_trolley),
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										Config.goods_in_trolley.remove(i);
										SharedPreferences preferences = getSharedPreferences("trolley", 0);
										Editor editor = preferences.edit();
										editor.putString("trolley", Config.toXML(Config.goods_in_trolley));
										editor.commit();
										Intent intent = new Intent();
										intent.setClass(TrolleyActivity.this,
												TrolleyActivity.class);
										startActivity(intent);
										finish();
										// data = get_goods_in_trolley();
										// adapter.notifyDataSetChanged();
									}
								}).show();
				return true;
			}
		});

		listview.setAdapter(adapter);
	}

	/**
	 * bundle列表转成map列表
	 * @return
	 */
	private List<? extends Map<String, ?>> get_goods_in_trolley() {
		List<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
		for (int i = 0; i < Config.goods_in_trolley.size(); i++) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			Iterator<String> it = Config.goods_in_trolley.get(i).keySet().iterator();
			while (it.hasNext()) {
				String key = it.next();
				map.put(key, Config.goods_in_trolley.get(i).get(key));
			}
			maplist.add(map);
		}
		return maplist;
	}


	
	
	private void setAddupto() {
		float addupto = 0;
		for (int i = 0; i < data.size(); i++) {
			addupto = (int) data.get(i).get("number")
					* Float.parseFloat((String) data.get(i).get("price_float"))  + addupto;
		}
		textview.setText(String.valueOf(addupto));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
