package net.feling.hq711;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

/**
 * 
 * @author chenyan@feling.net
 * 
 *         商品详情的界面， 返回时需要finish
 */
public class GoodsDetailActivity extends Activity {

	private Bundle goods;
	private WebView webview_goodsdetail;
	private ProgressBar progressbar;
	private Button button_trolley;
	private Button button_order;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_goods_detail);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		goods = getIntent().getBundleExtra("goods");
		button_trolley = (Button) this
				.findViewById(R.id.Button_trolley_goodsdetail);
		button_trolley.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Boolean dump = false;
				for (int i = -0; i < Config.goods_in_trolley.size(); i++) {
					if (goods.getString("goods_name").equals(
							Config.goods_in_trolley.get(i).getString("goods_name"))) {
						dump = true;
					}
				}
				if (!dump) {
					goods.putInt("number", 1);
					Config.goods_in_trolley.add(goods);
					SharedPreferences preferences = getSharedPreferences(
							"trolley", 0);
					Editor editor = preferences.edit();
					editor.putString("trolley",
							Config.toXML(Config.goods_in_trolley));
					editor.commit();
					Toast.makeText(GoodsDetailActivity.this,
							R.string.add_to_trolley_ok, Toast.LENGTH_SHORT)
							.show();
				} else {
					Toast.makeText(GoodsDetailActivity.this,
							R.string.already_in_trolley, Toast.LENGTH_SHORT)
							.show();
				}
			}
		});

		button_order = (Button) this
				.findViewById(R.id.Button_order_goodsdetail);
		button_order.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(GoodsDetailActivity.this, OrderActivity.class);
				goods.putInt("number", 1);
				ArrayList<Bundle> goodslist = new ArrayList<Bundle>();
				goodslist.add(goods);
				Bundle goodslist_bundle = new Bundle();
				goodslist_bundle.putSerializable("goodslist", goodslist);
				intent.putExtra("goodslist_bundle", goodslist_bundle);
				GoodsDetailActivity.this.startActivity(intent);
			}
		});

		progressbar = (ProgressBar) this
				.findViewById(R.id.ProgressBar_goodsdetail);
		webview_goodsdetail = (WebView) this
				.findViewById(R.id.WebView_goodsdetail);
		webview_goodsdetail.getSettings().setDefaultTextEncodingName("utf8");
		// webview_goodsdetail.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		final Activity activity = this;
		webview_goodsdetail.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
				activity.setProgress(progress * 100);
				progressbar.setProgress(progress);
				if (progress == 100) {
					progressbar.setProgress(0);
				}
			}
		});
		webview_goodsdetail.loadUrl(goods.getString("goods_detail"));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.goodslist, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.action_trolley:
			Intent intent = new Intent();
			intent.setClass(GoodsDetailActivity.this, TrolleyActivity.class);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}