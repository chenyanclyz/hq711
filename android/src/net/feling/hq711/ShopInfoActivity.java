package net.feling.hq711;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;

/**
 * 
 * @author chenyan@feling.net
 * 
 * 商家信息界面，
 * 返回时需要finish
 */
public class ShopInfoActivity extends Activity {
	
	private ProgressBar progressbar;
	private WebView webview_shopinfo;
	private Config config;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shop_info);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		config = (Config) getApplication();
		progressbar = (ProgressBar)this.findViewById(R.id.ProgressBar_shopinfo);
		webview_shopinfo = (WebView) this.findViewById(R.id.WebView_shopinfo);
		webview_shopinfo.getSettings().setDefaultTextEncodingName("utf8");
//		webview_shopinfo.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		final Activity activity = this;
		webview_shopinfo.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
				activity.setProgress(progress * 100);
				progressbar.setProgress(progress);
				if (progress == 100) {
					progressbar.setProgress(0);
				}
			}
		});
		webview_shopinfo.loadUrl(config.getServer() + "html/shop_info.html");
		
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.trolley, menu);
//		return true;
//	}
//
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
			
		}
		return super.onOptionsItemSelected(item);
	}
}