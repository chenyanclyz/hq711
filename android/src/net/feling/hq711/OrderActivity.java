package net.feling.hq711;

import java.util.ArrayList;
import java.util.List;

import net.feling.hq711.web.Mail;
import android.R.color;
import android.R.integer;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

/**
 * @author chenyan@feling.net
 * 
 * 确认订单的界面，
 * 返回时需要finish
 */
public class OrderActivity extends Activity {

	private ArrayList<Bundle> goodslist;
	private EditText name;
	private EditText phone;
	private EditText address;
	private WebView webview;
	private Button button;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_order);
		webview = (WebView)this.findViewById(R.id.WebView_order);
		webview.getSettings().setJavaScriptEnabled(true);
		webview.setFocusable(false);
		webview.setBackgroundColor(color.background_dark);
		webview.getSettings().setDefaultTextEncodingName("utf8");
		
		goodslist = (ArrayList<Bundle>) getIntent().getBundleExtra("goodslist_bundle").get("goodslist");
		webview.loadDataWithBaseURL(null, makeOrderTableHtml(goodslist), "text/html", "utf8", null);

		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		name = (EditText) this.findViewById(R.id.EditText_name_order);
		phone = (EditText) this.findViewById(R.id.EditText_phone_order);
		address = (EditText) this.findViewById(R.id.EditText_address_order);
		
		button = (Button)this.findViewById(R.id.Button_order);
		button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (check_input()){
				new Mail(OrderActivity.this, "新订单", getOrderMeg()).set_order();
				}else{
					Toast.makeText(OrderActivity.this, R.string.orderinfo_not_comp, Toast.LENGTH_SHORT).show();
				}
			}
		});
		
	}
	
	private Boolean check_input(){
		if (name.getText().length()>0 && phone.getText().length()>0 && address.getText().length()>0){
			return true;
		}else{
			return false;
		}
	}

	private String getOrderMeg(){
		String TRs = "";
		float addupto = 0;
		for(int i=0;i<goodslist.size();i++){
		String goods_name = (String) goodslist.get(i).get("goods_name");
		int goods_number = (int) goodslist.get(i).get("number");
		String goods_detail = (String)  goodslist.get(i).get("goods_detail");
		String goods_price = (String) goodslist.get(i).get("price");
		float goods_price_float = Float.parseFloat((String) goodslist.get(i).get("price_float"));
		if (i % 2 ==0){
			TRs = TRs + "<tr><td><a href='"
					+ goods_detail
					+ "'>"
					+ goods_name
					+ "</a></td><td>"
					+ goods_price
					+ "</td><td>"
					+ String.valueOf(goods_number)
					+ "</td><td>"
					+ String.valueOf(goods_price_float * goods_number)
					+ "</td></tr>";
		}else{
				TRs = TRs + "<tr bgcolor='#cccccc'><td><a href='"
						+ goods_detail
						+ "'>"
						+ goods_name
						+ "</a></td><td>"
						+ goods_price
						+ "</td><td>"
						+ String.valueOf(goods_number)
						+ "</td><td>"
						+ String.valueOf(goods_price_float * goods_number)
						+ "</td></tr>";
		}
		addupto = addupto + (goods_price_float * goods_number);
		}
		String html =  "<html><body>"
				+ "<table style='width:100%' border='1' cellpadding='0' cellspacing='0' bordercolor='#59ACFF'>"
				+ "<tr bgcolor='#59ACFF' ><td>商品</td><td>单价</td><td>数量</td><td>小计</td></tr>"
				+ TRs
				+ "</table>"
				+"<p>总计："
				+ String.valueOf(addupto)
				+ "</p>"
				+ "<br /><br /><br /></body></html>";
		
		String OrderMeg =
				"\n\n姓名：" + name.getText().toString() + 
				"\n\n电话：" + phone.getText().toString() + 
				"\n\n地址：" + address.getText().toString() + 
				"\n\n商品：\n\n" + html;
		return OrderMeg;
	}
	
	private String makeOrderTableHtml(ArrayList<Bundle> goodslist){
		String TRs = "";
		float addupto = 0;
		for(int i=0;i<goodslist.size();i++){
		String goods_name = (String) goodslist.get(i).get("goods_name");
		int goods_number = (int) goodslist.get(i).get("number");
		String goods_price = (String) goodslist.get(i).get("price");
		float goods_price_float = Float.parseFloat((String) goodslist.get(i).get("price_float"));
		if (i % 2 ==0){
			TRs = TRs + "<tr><td>"
					+ goods_name
					+ "</td><td>"
					+ goods_price
					+ "</td><td>"
					+ String.valueOf(goods_number)
					+ "</td><td>"
					+ String.valueOf(goods_price_float * goods_number)
					+ "</td></tr>";
		}else{
		TRs = TRs + "<tr bgcolor='#cccccc'><td>"
				+ goods_name
				+ "</td><td>"
				+ goods_price
				+ "</td><td>"
				+ String.valueOf(goods_number)
				+ "</td><td>"
				+ String.valueOf(goods_price_float * goods_number)
				+ "</td></tr>";
		}
		addupto = addupto + (goods_price_float * goods_number);
		}
		String html =  "<html><body>"
				+ "<table style='width:100%' border='1' cellpadding='0' cellspacing='0' bordercolor='#59ACFF'>"
				+ "<tr bgcolor='#59ACFF' ><td>商品名称</td><td>单价</td><td>数量</td><td>小计</td></tr>"
				+ TRs
				+ "</table>"
				+"<p>总计："
				+ String.valueOf(addupto)
				+ "</p>"
				+ "<br /><br /><br /></body></html>";
				return html;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
}
