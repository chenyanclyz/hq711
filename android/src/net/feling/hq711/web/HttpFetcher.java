package net.feling.hq711.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;

public class HttpFetcher {

	public HttpFetcher() {
	}

	public InputStream postStream(String url) {
		// String DECODE = "UTF8";
		InputStream inputstream = null;
		// BufferedReader reader = null;
		// String result = "";
		// String line = "";
		HttpClient httpclient = new DefaultHttpClient();
		httpclient.getParams().setParameter(
				CoreConnectionPNames.CONNECTION_TIMEOUT, 3000);
		httpclient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT,
				3000);
		HttpPost httpPost = new HttpPost(url);
		try {
			// httpPost.setEntity(new UrlEncodedFormEntity(null));
			HttpResponse response = httpclient.execute(httpPost);
			if (response.getStatusLine().getStatusCode() == 200) {
				inputstream = response.getEntity().getContent();
			}
			// reader = new BufferedReader(new InputStreamReader(inputstream,
			// DECODE));
			// if (response.getStatusLine().getStatusCode() == 200) {
			// while ((line = reader.readLine()) != null) {
			// result = result + line;
			// }
			// }
		} catch (Exception e) {e.printStackTrace();
			return null;
		} 
		return inputstream;
	}

	public InputStream getStream(String url) {
		InputStream inputstream = null;
		HttpClient httpclient = new DefaultHttpClient();
		httpclient.getParams().setParameter(
				CoreConnectionPNames.CONNECTION_TIMEOUT, 3000);
		httpclient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT,
				3000);
		HttpGet httpGet = new HttpGet(url);
		try {
			HttpResponse response = httpclient.execute(httpGet);
			if (response.getStatusLine().getStatusCode() == 200) {
				inputstream = response.getEntity().getContent();
			}
		} catch (Exception e) {
			e.printStackTrace();
//			inputstream = null;
		}
		return inputstream;
	}

	public String getString(String url) {
		String result = "";//初始化时，不能是null，null会被加到结果字符串中
		BufferedReader reader = null;
		String line = null;
		InputStream in = this.getStream(url);
		if (in == null) {
			result = null;
		} else {
			try {
				reader = new BufferedReader(new InputStreamReader(in));
				while ((line = reader.readLine()) != null) {
					result = result + line;
				}
			} catch (Exception e) {
				e.printStackTrace();
				result = null;
			} finally {
				if (in != null && reader != null) {
					try {
						in.close();
						reader.close();
					} catch (IOException e) {e.printStackTrace();
						result = null;
					}
				}
			}
		}
//		if (result != null){
//		System.out.println(result);
//		}
		return result;
	}
}
