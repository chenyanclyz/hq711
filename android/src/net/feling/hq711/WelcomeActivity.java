package net.feling.hq711;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.CountDownTimer;

public class WelcomeActivity extends Activity {
	
	/**
	 * @author chenyan@feling.net
	 * 每次打开程序时，出现 logo图片的界面
	 */
	private Intent intent = new Intent();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_welcome);
		SharedPreferences preferences = getSharedPreferences("first_pref", MODE_PRIVATE);
		// 取得相应的值，如果没有该值，说明还未写入，用true作为默认值
		boolean isFirstIn = preferences.getBoolean("isFirstIn", true);

		if (isFirstIn) {
			Editor editor = preferences.edit();
			editor.putBoolean("isFirstIn", false);
			editor.commit();
			intent.setClass(WelcomeActivity.this, GuidActivity.class);
		} else {
			intent.setClass(WelcomeActivity.this, GoodslistActivity.class);
		} 

		// 1秒后跳转
		TimeCount timecount = new TimeCount(1000, 1000);
		timecount.start();

	}

	private class TimeCount extends CountDownTimer {
		public TimeCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		public void onFinish() {
			startActivity(intent);
			finish();
		}

		public void onTick(long millisUntilFinished) {

		}
	}


//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.welcome, menu);
//		return true;
//	}

//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		// Handle action bar item clicks here. The action bar will
//		// automatically handle clicks on the Home/Up button, so long
//		// as you specify a parent activity in AndroidManifest.xml.
//		int id = item.getItemId();
//		if (id == R.id.action_settings) {
//			return true;
//		}
//		return super.onOptionsItemSelected(item);
//	}
}
