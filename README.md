###便利店的下单客户端
###适合小型商店（主要局限是服务端以文件方式维护，客户端图片缓存在内存中没有考虑清理）
作者邮箱： chenyan@feling.net    

#目录结构
+ android/    
客户端的源代码，包含了eclipse工程里的所有文件，所以可以直接导入，在里面的bin文件夹下可以找到apk文件    
+ hq711/    
服务端文件，因为没有太多的时间，服务端的程序没有写。所以用手动上传商品图片，编辑商品列表的方式维护服务端。客户端下单后，通过邮件通知店主。   
+ server_no_use/    
原本打算做服务端的程序，现在不用服务端了。服务端替换为普通web服务器+hq711文件夹的方案。    


#开发环境
字符编码：utf8   
换行符：\n   
ubuntu14.04 64位 eclipse + ADT + java1.7.0_71   
Android SDK Tools 24.0.2    
Android SDK Platform-tools 21    
Adnroid SDK Buil-tools 21.1.2    
SDK Platform 19(API 19）    
Adnroid Support Library 21.0.3    
Adnroid Support Repository 11    


