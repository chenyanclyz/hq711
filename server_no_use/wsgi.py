#!/usr/bin/python
# coding:utf-8

import logging; logging.basicConfig(level=logging.INFO)
import os, time

from feling import db
from feling.web import WSGIApplication

from config import configs

db.create_engine(**(configs.db))

wsgi = WSGIApplication(os.path.dirname(os.path.abspath(__file__)))
from engine import template_engine
wsgi.template_engine = template_engine

import urls
wsgi.add_module('urls')
wsgi.add_interceptor(urls.user_interceptor)
wsgi.add_interceptor(urls.manage_interceptor)

if __name__ == '__main__':
    wsgi.run(9000)
else:
    application = wsgi.get_wsgi_application(debug=True)
