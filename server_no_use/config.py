#!/usr/bin/python
# coding:utf-8

# config.py
import config_default
from feling._dict import Dict

def merge(defaults, override):
    r = {}
    for k, v in defaults.iteritems():
        if k in override:
            if isinstance(v, dict):
                r[k] = merge(v, override[k])
            else:
                r[k] = override[k]
        else:
            r[k] = v
    return r


configs = config_default.configs

try:
    import config_override
    configs = merge(configs, config_override.configs)
except ImportError,e:
    print e    

configs = Dict(**configs)