#!/usr/bin/python
# coding:utf-8

import time, uuid

from feling.db import next_id
from feling.orm import Model, StringField, BooleanField, FloatField, TextField, IntegerField

class User(Model):
    __table__ = 'users'

    id = StringField(primary_key=True, default=next_id, ddl='varchar(50)')
    email = StringField(updatable=False, ddl='varchar(50)')
    phone = StringField(ddl='varchar(11)')
    password = StringField(ddl='varchar(50)')
    admin = BooleanField()
    name = StringField(ddl='varchar(50)')
    image = StringField(ddl='varchar(500)')
    created_at = FloatField(updatable=False, default=time.time)

class Goods(Model):
    __table__ = 'goods'

    id = StringField(primary_key=True, default=next_id, ddl='varchar(50)')
    goods_name = StringField(ddl='varchar(50)')
    goods_image = StringField(ddl='varchar(500)')
    introduction = StringField(ddl='varchar(200)')
    price = StringField(ddl='varchar(15)')
    belongs = StringField(ddl='varchar(50)')
    created_at = FloatField(updatable=False, default=time.time)

class Order(Model):
    __table__ = 'orders'
    id = StringField(primary_key=True, default=next_id, ddl='varchar(50)')
    phone = StringField(ddl='varchar(11)')
    address = TextField()
    user_name = StringField(ddl='varchar(50)')
    postal_code = StringField(ddl='varchar(6)')
    remarks = StringField(ddl='varchar(500)')
    #json格式的已选商品详情
    goods_count = TextField()
    created_at = FloatField(updatable=False, default=time.time)
#class Comment(Model):
#    __table__ = 'comments'

#    id = StringField(primary_key=True, default=next_id, ddl='varchar(50)')
#    blog_id = StringField(updatable=False, ddl='varchar(50)')
#    user_id = StringField(updatable=False, ddl='varchar(50)')
#    user_name = StringField(ddl='varchar(50)')
#    user_image = StringField(ddl='varchar(500)')
#    content = TextField()
#    created_at = FloatField(updatable=False, default=time.time)
